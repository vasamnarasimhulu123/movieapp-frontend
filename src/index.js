import React from "react";
import ReactDom from "react-dom";
import App from "./Components/App";
import "./stylesheets/style.css";
import { BrowserRouter } from "react-router-dom";
ReactDom.render(
  <BrowserRouter>
    <App />
  </BrowserRouter>,
  document.getElementById("root")
);

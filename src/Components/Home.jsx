import React from "react";
import Hero from "./Hero";
import SampleCards from "./SampleCards";
import { NavLink } from "react-router-dom";
import { withRouter } from "react-router-dom/cjs/react-router-dom.min";
import AddedCards from "./AddedCards";

function Home(props) {
  let movieData = props;
  return (
    <div className="Home">
      <Hero />
      <SampleCards movieData={movieData} />
      <AddedCards movieData={movieData} usersDetails={props.usersDetails} />
      <NavLink to={props?.usersDetails?.user_id ? "/addmovie" : "/signin"}>
        <div className="AddMovie flex justify_center items_center">
          <p>+</p>
        </div>
      </NavLink>
    </div>
  );
}

export default withRouter(Home);

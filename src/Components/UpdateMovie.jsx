import React from "react";
import { api_base } from "../utils";
import { withRouter, NavLink } from "react-router-dom/cjs/react-router-dom.min";

class UpdateMovie extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      movie_name: "",
      movie_image: "",
      description: "",
      director: "",
    };
  }
  componentDidMount() {
    this.FetchEachArticle();
  }
  FetchEachArticle = () => {
    var { slug } = this.props.match.params;
    console.log(slug, "slug  in indi");
    fetch(api_base + `movies` + `/${slug}`)
      .then((res) => {
        return res.json();
      })
      .then((articleData) => {
        this.setState({
          movie_name: articleData[0].movie_name,
          movie_image: articleData[0].movie_image,
          description: articleData[0].description,
          director: articleData[0].director,
        });
      })
      .catch((err) => console.log(err));
  };
  fetchUpdate = () => {
    var { slug } = this.props.match.params;
    fetch(api_base + `movies/` + slug, {
      method: "PUT", // *GET, POST, PUT, DELETE, etc.
      mode: "cors", // no-cors, *cors, same-origin
      cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
      credentials: "same-origin", // include, *same-origin, omit
      headers: {
        "Content-Type": "application/json",
        // 'Content-Type': 'application/x-www-form-urlencoded',
      },
      redirect: "follow", // manual, *follow, error
      referrerPolicy: "no-referrer", // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
      body: JSON.stringify({
        movie_name: this.state.movie_name,
        movie_image: this.state.movie_image,
        description: this.state.description,
        director: this.state.director,
      }), // body data type must match "Content-Type" header
    })
      .then((res) => res.json())
      .then((data) => {
        this.props.history.push(`/movies/${slug}`);
      });
  };
  handleChange = ({ target }) => {
    var { name, value } = target;
    this.setState({
      [name]: value,
    });
  };
  handleSubmit = (event) => {
    event.preventDefault();
    this.fetchUpdate();
  };
  render() {
    var { eachMovie } = this.state;
    return (
      <>
        <div className="container flex  items_center justify_center signInpage">
          <div className="flex_43 signin_imageSEC">
            <lottie-player
              src="https://assets9.lottiefiles.com/packages/lf20_qm8eqzse.json"
              background="transparent"
              speed="1"
              style={{ width: "100%" }}
              loop
              autoplay
            ></lottie-player>
          </div>

          <form action="" className="flex_43" onSubmit={this.handleSubmit}>
            <fieldset>
              <h2 className="heading_signin">Update Movie</h2>
              <div className="setInput">
                <label htmlFor="">Movie Name</label>
                <input
                  onChange={(e) => this.handleChange(e)}
                  name="movie_name"
                  value={this.state.movie_name}
                  type="text"
                  placeholder="Enter Your Movie Name"
                />
              </div>
              <div className="setInput">
                <label htmlFor="">Movie Image</label>
                <input
                  onChange={(e) => this.handleChange(e)}
                  name="movie_image"
                  value={this.state.movie_image}
                  type="text"
                  placeholder="Enter Your Movie Image"
                />
              </div>
              <div className="setInput">
                <label htmlFor="">Description</label>
                <input
                  onChange={(e) => this.handleChange(e)}
                  name="description"
                  value={this.state.description}
                  type="text"
                  placeholder="Enter Your Password"
                />
              </div>
              <div className="setInput">
                <label htmlFor="">Director</label>
                <input
                  onChange={(e) => this.handleChange(e)}
                  name="director"
                  value={this.state.director}
                  type="text"
                  placeholder="Enter Your Password"
                />
              </div>

              <div className="buttons flex items_center">
                <button>Submit</button>
              </div>
            </fieldset>
          </form>
        </div>
      </>
    );
  }
}
export default withRouter(UpdateMovie);

import React from "react";
import { NavLink } from "react-router-dom";
import { withRouter } from "react-router-dom/cjs/react-router-dom.min";
import { api_base } from "../utils";
class AddMovie extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      movie_name: "",
      movie_image: "",
      description: "",
      director: "",
      users_id: null,
    };
  }
  fetchDetails = () => {
    var storagekey = localStorage["apiKey"];
    if (storagekey) {
      fetch(api_base + `movies`, {
        method: "POST", // *GET, POST, PUT, DELETE, etc.
        mode: "cors", // no-cors, *cors, same-origin
        cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
        credentials: "same-origin", // include, *same-origin, omit
        headers: {
          "Content-Type": "application/json",
          authorization: `Token ${storagekey}`,
          // 'Content-Type': 'application/x-www-form-urlencoded',
        },
        redirect: "follow", // manual, *follow, error
        referrerPolicy: "no-referrer", // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
        body: JSON.stringify({
          movie_name: this.state.movie_name,
          movie_image: this.state.movie_image,
          description: this.state.description,
          director: this.state.director,
        }), // body data type must match "Content-Type" header
      })
        .then((res) => res.json())
        .then((data) => {
          this.setState({
            movie_name: "",
            movie_image: "",
            description: "",
            director: "",
          });
        });
    }
  };
  componentDidUpdate(__prevProps, prevState) {
    //Typical usage, don't forget to compare the props
    if (__prevProps.movieData !== this.state.movieData) {
      return this.props.fetchMovies();
    }
  }
  componentWillUnmount() {
    return () => {};
  }
  handleChange = ({ target }) => {
    var { name, value } = target;
    this.setState({
      [name]: value,
    });
  };
  handleSubmit = (event) => {
    event.preventDefault();
    this.fetchDetails();
  };
  render() {
    var { movieData, usersDetails } = this.props;

    return (
      <>
        <div className="AddMovieSec">
          <div className="container flex items_center justify_between column">
            <div className="flex_43 AddMovieSec_left">
              <img
                className="addMovieImage"
                src="/images/addmovie.svg"
                alt=""
              />
              <form action="" onSubmit={this.handleSubmit}>
                <fieldset>
                  <h2 className="heading_signin">Add Movie</h2>
                  <div className="setInput">
                    <label htmlFor="">Movie Name</label>
                    <input
                      onChange={(e) => this.handleChange(e)}
                      name="movie_name"
                      value={this.state.movie_name}
                      type="text"
                      placeholder="Enter Your Movie Name"
                    />
                  </div>
                  <div className="setInput">
                    <label htmlFor="">Movie Image</label>
                    <input
                      onChange={(e) => this.handleChange(e)}
                      name="movie_image"
                      value={this.state.movie_image}
                      type="text"
                      placeholder="Enter Your Movie Image"
                    />
                  </div>
                  <div className="setInput">
                    <label htmlFor="">Description</label>
                    <input
                      onChange={(e) => this.handleChange(e)}
                      name="description"
                      value={this.state.description}
                      type="text"
                      placeholder="Enter Your Password"
                    />
                  </div>
                  <div className="setInput">
                    <label htmlFor="">Director</label>
                    <input
                      onChange={(e) => this.handleChange(e)}
                      name="director"
                      value={this.state.director}
                      type="text"
                      placeholder="Enter Your Password"
                    />
                  </div>

                  <div className="buttons flex items_center">
                    <button>Submit</button>
                  </div>
                </fieldset>
              </form>
            </div>
            <div className="flex_43 AddMovieRightSec">
              {movieData.map((each) => (
                <>
                  {usersDetails?.user_id == each?.users_id ? (
                    <NavLink to={`/movies/${each.movie_id}`}>
                      <div class="Card1" key={each.user_id}>
                        <div
                          class="photo"
                          style={
                            each.movie_image !== ""
                              ? { backgroundImage: `url(${each.movie_image})` }
                              : {
                                  backgroundImage: `url(https://cringemdb.com/img/movie-poster-placeholder.png)`,
                                }
                          }
                        ></div>
                        <div class="description">
                          <div class="line">
                            <h2 class="product_name">{each.movie_name}</h2>
                          </div>
                          <h2>{each.director}</h2>
                          <p class="summary">{each.description}</p>
                          <a href="//s.codepen.io/ImagineAlex">Read More</a>
                        </div>
                      </div>
                    </NavLink>
                  ) : (
                    ""
                  )}
                </>
              ))}
            </div>
          </div>
          <NavLink to="/">
            <div className="listAllMovies flex justify_center items_center">
              <p>List Movies</p>
            </div>
          </NavLink>
        </div>
      </>
    );
  }
}
export default withRouter(AddMovie);

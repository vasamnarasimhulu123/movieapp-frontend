import React from "react";

function NotFound() {
  return (
    <div className="container NotFound flex justify_center items_center">
      <lottie-player
        src="https://assets2.lottiefiles.com/packages/lf20_inuxiflu.json"
        background="transparent"
        speed="1"
        style={{ width: "65%" }}
        loop
        autoplay
      ></lottie-player>
    </div>
  );
}

export default NotFound;

import React from "react";
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from "react-responsive-carousel";

function Hero() {
  return (
    <div className="hero">
      <Carousel
        autoPlay={true}
        infiniteLoop={true}
        showStatus={false}
        showThumbs={false}
        stopOnHover={false}
      >
        <div>
          <img
            className="carousel_image"
            src="/images/home/image1.jpg"
            alt="imagename"
          />
          <div className="Movie_Details container">
            <h2>The Last Night</h2>
            <p className="description">
              The film follows married couple Joanna Reed (Keira Knightley) and
              Michael Reed (Sam Worthington), who are tempted by different forms
              of infidelity when they spend a night apart following a fight. ...
              The film was developed as a romance and suspense story before
              Tadjedin recognized its moral significance.
            </p>
            <p className="direction_dep">
              <span> Director( Gaumont (France))</span>{" "}
              <span>Produced by Sidonie Dumas and Massy Tadjedin;</span>
            </p>
          </div>
        </div>
        <div>
          <img
            className="carousel_image"
            src="/images/home/image2.jpg"
            alt="imagename"
          />
          <div className="Movie_Details container">
            <h2>The Joker</h2>
            <p className="description">
              In Gotham City, mentally troubled comedian Arthur Fleck is
              disregarded and mistreated by society. He then embarks on a
              downward spiral of revolution and bloody crime. This path brings
              him face-to-face with his alter-ego: the Joker. Arthur Fleck works
              as a clown and is an aspiring stand-up comic.Plot — While
              Phoenix's performance, the direction, editing, musical score, and
              cinematography were praised, the dark tone, portrayal of mental
              illness
            </p>
            <p className="direction_dep">
              <span> Director( Gaumont (France))</span>{" "}
              <span>Produced by Sidonie Dumas and Massy Tadjedin;</span>
            </p>
          </div>
        </div>
        <div>
          <img
            className="carousel_image"
            src="/images/home/image3.jpg"
            alt="imagename"
          />
          <div className="Movie_Details container">
            <h2>Rampage</h2>
            <p className="description">
              It follows a primatologist who must team up with George, an albino
              western lowland gorilla who turns into a raging creature of
              enormous size as a result of a rogue experiment, to stop two other
              mutated animals from destroying Chicago.
            </p>
            <p className="direction_dep">
              <span> Director( Gaumont (France))</span>{" "}
              <span>Produced by: Dwayne Johnson; Brad Peyton;</span>
            </p>
          </div>
        </div>
        <div>
          <img
            className="carousel_image"
            src="/images/home/image4.jpg"
            alt="imagename"
          />
          <div className="Movie_Details container">
            <h2>Chimpanzee</h2>
            <p className="description">
              Deep in an African forest lives a family of chimpanzees, including
              a baby named Oscar. Displaying the playful curiosity and ingenuity
              of fellow primates, Oscar and his family navigate the forest's
              complex territory with relative ease.
            </p>
            <p className="direction_dep">
              <span> Director (Alastair Fothergill, Mark Linfield)</span>{" "}
              <span>Produced by Alastair Fothergill, Mark Linfield, Alix</span>
            </p>
          </div>
        </div>
        <div>
          <img
            className="carousel_image"
            src="/images/home/image5.jpg"
            alt="imagename"
          />
          <div className="Movie_Details container">
            <h2>Captain America</h2>
            <p className="description">
              Captain America is a superhero appearing in American comic books
              published by Marvel Comics. Created by cartoonists Joe Simon and
              Jack Kirby, the character first appeared in Captain America Comics
              #1 (cover dated March 1941) from Timely Comics, a predecessor of
              Marvel Comics. Captain America was designed as a patriotic
              supersoldier who often fought the Axis powers of World War II and
              was Timely Comics' most popular character during the wartime
              period
            </p>
            <p className="direction_dep">
              <span> Director( Gaumont (France))</span>{" "}
              <span>Produced by Sidonie Dumas and Massy Tadjedin;</span>
            </p>
          </div>
        </div>
      </Carousel>
    </div>
  );
}

export default Hero;

import { render } from "@testing-library/react";
import React from "react";
import Home from "./Home";
import { Route, Switch, withRouter } from "react-router-dom";
import Signin from "./Signin";
import Register from "./Register";
import AddMovie from "./AddMovie";
import { api_base, userVerify } from "../utils";
import EachArticle from "./EachArticle";
import IndividualMovie from "./IndividualMovie";
import UpdateMovie from "./UpdateMovie";
import Header from "./Header";
import NotFound from "./NotFound";
class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      movieData: [],
      isLogged: false,
      users: null,
      isVerifying: false,
      isLoading: true,
    };
  }
  componentDidMount() {
    localStorage.getItem("apiKey");
    this.fetchMovies();
    this.verifyUser();
  }

  verifyUser = () => {
    let storagekey = localStorage["apiKey"];
    if (storagekey) {
      fetch(userVerify, {
        method: "GET",
        headers: {
          authorization: `${storagekey}`,
        },
      })
        .then((res) => res.json())
        .then((user) => {
          this.setState({
            isVerifying: !this.state.isVerifying,
            users: user[0],
            isLoading: false,
          });
        })
        // .then((res) => {
        //   if (res.ok) {
        //     return res.json();
        //   }
        //   return res.json().then(({ errors }) => {
        //     return Promise.reject(errors);
        //   });
        // })
        // .then((user) => {
        //   console.log(user);
        //   this.setState({
        //     isVerifying: true,
        //   });
        // })
        .catch((errors) => console.log(errors));
    } else {
      this.setState({
        isVerifying: false,
      });
    }
  };
  fetchMovies = () => {
    fetch(`${api_base}movies`)
      .then((res) => res.json())
      .then((result) => {
        this.setState({
          movieData: result,
          isLoading: false,
        });
      });
  };
  handleLogout = () => {
    localStorage.clear();
  };
  userData = (data) => {
    this.setState({
      isLogged: !this.state.isLogged,
      users: data.user,
    });
  };

  render() {
    var { movieData, isLogged, token, isVerifying, users } = this.state;
    if (this.state.isLoading) {
      return (
        <>
          <div className="loading_indicator flex justify_center items_center">
            <lottie-player
              src="https://assets2.lottiefiles.com/packages/lf20_yU09RI.json"
              background="transparent"
              speed="1"
              style={{ width: "10%", marginTop: "5rem" }}
              loop
              autoplay
              className="hello"
            ></lottie-player>
          </div>
        </>
      );
    }
    return (
      <>
        <Header
          isLogged={isLogged}
          isVerifying={isVerifying}
          handleLogout={this.handleLogout}
          users={users}
        />
        <Switch>
          <Route path="/" exact>
            <Home movieData={movieData} usersDetails={this.state.users} />
          </Route>
          <Route path="/signin" exact>
            <Signin
              userData={this.userData}
              state={this.state}
              fetchMovies={this.fetchMovies}
              verifyUser={this.verifyUser}
            />
          </Route>
          <Route path="/register" exact>
            <Register />
          </Route>
          <Route path="/addmovie" exact>
            <AddMovie
              movieData={movieData}
              fetchMovies={this.fetchMovies}
              usersDetails={this.state.users}
            />
          </Route>
          <Route path="/static/:slug" exact>
            <EachArticle movieData={movieData} />
          </Route>
          <Route path="/movies/:slug" exact>
            <IndividualMovie
              movieData={movieData}
              fetchMovies={this.fetchMovies}
              usersDetails={this.state.users}
            />
          </Route>
          <Route path="/movies/:slug/updateMovie" exact>
            <UpdateMovie movieData={movieData} />
          </Route>
          <Route path="*" exact>
            <NotFound />
          </Route>
        </Switch>
      </>
    );
  }
}
export default withRouter(App);

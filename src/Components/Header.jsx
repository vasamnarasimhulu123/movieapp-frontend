import React from "react";
import { NavLink, withRouter } from "react-router-dom";

function Header(props) {
  var { isLogged, users, isVerifying } = props;
  var handleLogout = () => {
    localStorage.clear();
    props.history.push("/");
  };
  return (
    <>
      {!users ? (
        <div className="header">
          <div className="container">
            <div className="signin_signup flex items_center justify_between">
              <figure>
                <NavLink to="/">
                  <img
                    className="header_logo"
                    src="/images/moviesapp.png"
                    alt=""
                  />
                </NavLink>
              </figure>
              <div className="flex items_center user_details">
                <p className="sign_in">
                  <NavLink to="/signin">SignIn </NavLink>
                </p>
                <p className="register">
                  <NavLink to="/register">Register</NavLink>
                </p>
              </div>
            </div>
          </div>
        </div>
      ) : (
        <div className="header">
          <div className="container">
            <div className="signin_signup flex items_center justify_between">
              <figure>
                <NavLink to="/">
                  <img
                    className="header_logo"
                    src="/images/moviesapp.png"
                    alt=""
                  />
                </NavLink>
              </figure>
              <div className="flex items_center user_details">
                <i class="fa-solid fa-circle-user userIcon"></i>
                <p>{users?.user_name.toUpperCase()}</p>
                <a
                  href="/signin"
                  className="logout"
                  onClick={props.handleLogout}
                >
                  logout
                </a>
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
}

export default withRouter(Header);

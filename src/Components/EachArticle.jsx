import React from "react";
import { withRouter } from "react-router-dom/cjs/react-router-dom.min";
import { api_base } from "../utils";
import sampleCards from "../samplecards.json";
class EachArticle extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    var { slug } = this.props.match.params;
    var Singledata = [];
    var eachdata = sampleCards.map((each) => {
      if (each.id == slug) {
        return Singledata.push(each);
      }
    });
    return (
      <>
        {Singledata.map((each) => (
          <>
            <div
              className="eachArticle flex justify_center"
              key={each.movie_id}
            >
              <div className="left flex_43 flex justify_center items_center">
                <div className="content">
                  <h2>{each.movie_name}</h2>
                  <p>{each.description}</p>
                </div>
              </div>
              <div className="right flex_43">
                <img
                  src={`/images/samplecards/${each.movie_image}.jpg`}
                  alt=""
                />
              </div>
            </div>
            <div className="conclusion_app flex items_center">
              <p>
                - - - - - - - Such a best experience to watch the movies and you
                will feel like a theatre{" "}
              </p>
              <img
                src="https://cdn-icons.flaticon.com/png/512/1655/premium/1655698.png?token=exp=1645420731~hmac=e8902c16fcf3dcc6e1c44fafb735962c"
                alt=""
              />
              <img
                src="https://cdn-icons-png.flaticon.com/512/1038/1038100.png"
                alt=""
              />
            </div>
          </>
        ))}
      </>
    );
  }
}
export default withRouter(EachArticle);

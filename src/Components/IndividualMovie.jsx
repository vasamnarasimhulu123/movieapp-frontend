import React from "react";
import { api_base } from "../utils";
import { withRouter, NavLink } from "react-router-dom/cjs/react-router-dom.min";

class IndividualMovie extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      eachMovie: [],
    };
  }
  componentDidMount() {
    this.FetchEachArticle();
  }
  FetchEachArticle = () => {
    var { slug } = this.props.match.params;
    fetch(api_base + `movies` + `/${slug}`)
      .then((res) => {
        return res.json();
      })
      .then((articleData) => {
        this.setState({
          eachMovie: articleData,
        });
      })
      .catch((err) => console.log(err));
  };
  FetchDelete = () => {
    var { slug } = this.props.match.params;
    fetch(api_base + `movies/` + slug, {
      method: "DELETE", // *GET, POST, PUT, DELETE, etc.
      mode: "cors", // no-cors, *cors, same-origin
      cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
      credentials: "same-origin", // include, *same-origin, omit
      headers: {
        "Content-Type": "application/json",
        // 'Content-Type': 'application/x-www-form-urlencoded',
      },
      redirect: "follow", // manual, *follow, error
      referrerPolicy: "no-referrer", // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
      body: JSON.stringify({}), // body data type must match "Content-Type" header
    })
      .then((res) => res.json())
      .then((data) => {
        this.props.history.push(`/`);
      });
  };
  handleDelete = () => {
    this.FetchDelete();
  };
  componentDidUpdate(__prevProps, prevState) {
    //Typical usage, don't forget to compare the props
    if (prevState.movieData !== this.state.movieData) {
      this.props.fetchMovies();
    }
  }
  componentWillUnmount() {
    this.props.fetchMovies();
    return () => {};
  }
  render() {
    const { eachMovie } = this.state;
    const { usersDetails } = this.props;
    return (
      <>
        {" "}
        {eachMovie.map((each) => (
          <>
            <div
              className="eachArticle flex justify_center"
              key={each.movie_id}
            >
              <div className="left flex_43 flex justify_center items_center ">
                <div className="content">
                  <h2>{each.movie_name}</h2>
                  <p>{each.description}</p>
                  {each.director ? <p>Director : {each.director}</p> : ""}
                  <div className="edit_delete_icons flex items_center">
                    {usersDetails?.user_id == each?.users_id ? (
                      <>
                        <NavLink to={`/movies/${each.movie_id}/UpdateMovie`}>
                          <button className="EditButton">Edit</button>
                        </NavLink>
                        <button
                          onClick={this.handleDelete}
                          className="DeleteButton"
                        >
                          Delete
                        </button>
                        <i
                          onClick={this.handleDelete}
                          class="fa-solid fa-circle-trash"
                        ></i>
                      </>
                    ) : (
                      ""
                    )}
                  </div>
                </div>
              </div>
              <div className="right flex_43 flex justify_center items_center">
                <div>
                  {each.movie_image ? (
                    <img
                      className="lottie"
                      src={each.movie_image}
                      alt={each.movie_name}
                    />
                  ) : (
                    <div className="lottie flex justify_center items_center">
                      <lottie-player
                        src="https://assets9.lottiefiles.com/packages/lf20_SqEf0A.json"
                        background="transparent"
                        speed="1"
                        style={{ width: "80%", marginTop: "5rem" }}
                        loop
                        autoplay
                      ></lottie-player>
                    </div>
                  )}
                </div>
              </div>
            </div>
            <div className="conclusion_app flex items_center">
              <p>
                - - - - - - - Such a best experience to watch the movies and you
                will feel like a theatre{" "}
              </p>
              <img
                src="https://cdn-icons.flaticon.com/png/512/1655/premium/1655698.png?token=exp=1645420731~hmac=e8902c16fcf3dcc6e1c44fafb735962c"
                alt=""
              />
              <img
                src="https://cdn-icons-png.flaticon.com/512/1038/1038100.png"
                alt=""
              />
            </div>
          </>
        ))}
      </>
    );
  }
}

export default withRouter(IndividualMovie);

import React from "react";
import { NavLink } from "react-router-dom";

function AddedCards(props) {
  var { movieData } = props;
  return (
    <div className="container">
      {movieData.movieData.length > 0 ? (
        <h2 className="AddedMovies">All Added Movies</h2>
      ) : (
        ""
      )}
      <div className="sample_cards flex items_center flex_wrap">
        {movieData.movieData.map((each) => (
          <>
            {props?.usersDetails?.user_id == each?.users_id ? (
              <div class="Card3" key={each.user_id}>
                <NavLink to={`/movies/${each.movie_id}`}>
                  <img
                    src={
                      each.movie_image == ""
                        ? `https://cringemdb.com/img/movie-poster-placeholder.png`
                        : each.movie_image
                    }
                    alt="hello"
                  />
                  <div class="overlay">
                    <div class="line">
                      <h2 class="title">{each.movie_name.toUpperCase()}</h2>
                    </div>
                    <div class="content">
                      <p>{each.description.slice(0, 170)} Read More . . . . </p>
                    </div>
                  </div>
                </NavLink>
              </div>
            ) : (
              ""
            )}
          </>
        ))}

        {/* All movies */}
        {movieData.movieData.map((each) => (
          <>
            {!props?.usersDetails?.user_id ? (
              <div class="Card3">
                <NavLink to={`/movies/${each.movie_id}`}>
                  <img
                    src={
                      each.movie_image == ""
                        ? `https://cringemdb.com/img/movie-poster-placeholder.png`
                        : each.movie_image
                    }
                    alt="hello"
                  />
                  <div class="overlay">
                    <div class="line">
                      <h2 class="title">{each.movie_name.toUpperCase()}</h2>
                    </div>
                    <div class="content">
                      <p>{each.description.slice(0, 170)} Read More . . . . </p>
                    </div>
                  </div>
                </NavLink>
              </div>
            ) : (
              ""
            )}
          </>
        ))}
      </div>
    </div>
  );
}

export default AddedCards;

import React from "react";
import sampleCards from "../samplecards.json";
import { NavLink } from "react-router-dom";

function SampleCards(props) {
  var { movieData } = props;
  return (
    <div className="sample_cards container flex items_center flex_wrap">
      {sampleCards.map((each) => (
        <>
          <div class="Card3" key={each.movie_name}>
            <NavLink to={`/static/${each.id}`}>
              <img
                src={`/images/samplecards/${each.movie_image}.jpg`}
                alt="hello"
              />

              <div class="overlay">
                <div class="line">
                  <h2 class="title">{each.movie_name.toUpperCase()}</h2>
                </div>
                <div class="content">
                  <p>{each.description.slice(0, 170)} </p>
                </div>
              </div>
            </NavLink>
          </div>
        </>
      ))}
    </div>
  );
}

export default SampleCards;

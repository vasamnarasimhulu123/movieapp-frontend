import React from "react";
import { withRouter, NavLink } from "react-router-dom";
import { api_base } from "../utils";

class Register extends React.Component {
  constructor() {
    super();
    this.state = {
      user_name: "",
      user_email: "",
      user_password: "",
    };
  }

  registerUser = () => {
    fetch(api_base + `users/register`, {
      method: "POST", // *GET, POST, PUT, DELETE, etc.
      mode: "cors", // no-cors, *cors, same-origin
      cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
      credentials: "same-origin", // include, *same-origin, omit
      headers: {
        "Content-Type": "application/json",
        // 'Content-Type': 'application/x-www-form-urlencoded',
      },
      redirect: "follow", // manual, *follow, error
      referrerPolicy: "no-referrer", // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
      body: JSON.stringify({
        user_name: this.state.user_name,
        user_email: this.state.user_email,
        user_password: this.state.user_password,
      }), // body data type must match "Content-Type" header
    })
      .then((res) => res.json())
      .then((data) => {
        this.setState({
          user_name: "",
          user_email: "",
          user_password: "",
        });
        this.props.history.push(`/signin`);
      });
  };

  handleChange = ({ target }) => {
    var { name, value } = target;
    this.setState({
      [name]: value,
    });
  };
  handleSubmit = (e) => {
    e.preventDefault();
    if (
      this.state.user_name == "" &&
      this.state.user_email == "" &&
      this.state.user_password == ""
    ) {
      return "";
    } else {
      this.registerUser();
    }
  };

  render() {
    return (
      <>
        <div className="container flex  items_center justify_center signInpage column">
          <div className="flex_43 signin_imageSEC">
            <lottie-player
              src="https://assets2.lottiefiles.com/packages/lf20_mjlh3hcy.json"
              background="transparent"
              speed="1"
              style={{ width: "100%" }}
              loop
              autoplay
            ></lottie-player>
          </div>

          <form action="" onSubmit={this.handleSubmit} className="flex_35 form">
            <fieldset>
              <h2 className="heading_signin">Register</h2>
              <div className="setInput">
                <label htmlFor="">UserName</label>
                <input
                  type="text"
                  value={this.state.user_name}
                  name="user_name"
                  onChange={(e) => this.handleChange(e)}
                  placeholder="Enter Your UserName"
                />
              </div>
              <div className="setInput">
                <label htmlFor="">Email</label>
                <input
                  type="text"
                  value={this.state.user_email}
                  name="user_email"
                  onChange={(e) => this.handleChange(e)}
                  placeholder="Enter Your Email"
                />
              </div>
              <div className="setInput">
                <label htmlFor="">Password</label>
                <input
                  type="password"
                  name="user_password"
                  value={this.state.user_password}
                  onChange={(e) => this.handleChange(e)}
                  placeholder="Enter Your Password"
                />
              </div>

              <div className="buttons flex items_center">
                <button>Submit</button>
                <p>
                  <NavLink to="/signin">SignIn</NavLink>
                </p>
              </div>
            </fieldset>
          </form>
        </div>
      </>
    );
  }
}

export default withRouter(Register);
